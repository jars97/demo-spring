package com.example.demo.repository;

import org.springframework.stereotype.Repository;

import com.example.demo.entity.Notas;

@Repository
public interface NotasRepository extends CommonRepository<Notas>{

}
