package com.example.demo.dto;

import com.example.demo.entity.Facturas;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RenglonesDTO {
	private Long id;
	private Long renglonId;
}
