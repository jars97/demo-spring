package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BancosDTO {
	private Long id;
	private String name;
}
